"""my_city view Configuration

functions defined here will be accessed via urls.py
"""
from django.shortcuts import render, redirect
from django.http import HttpResponse

from users.views import *
from users.models import User
from users.models import Review

from city.models import *

# from geopy.geocoders import Nominatim
# import ssl

# ssl._create_default_https_context = ssl._create_unverified_context

def landing_page(request):
    return render(request, "index.html")
	
# The processing method for registration page
# Author: Jason
def register(request):
	# Dictionary for registration page
    reg_dict = {}
    if request.POST:
        allFieldsAreLegal = True

        # Check required inputs. When any of them are not filled, or not valid, the form will not be passed,
		# and some error massage will be shown.
        if not validateUsername(request.POST['username']):
            allFieldsAreLegal = False
            reg_dict['usernameError'] = "Please enter a valid username"
            reg_dict['usernameInput'] = request.POST['username']
        else:
            username = request.POST['username']

        if not validatePassword(request.POST['password']):
            allFieldsAreLegal = False
            reg_dict['passwordError'] = "Please enter a valid password"
            reg_dict['passwordInput'] = request.POST['password']
        else:
            password = request.POST['password']

        if not checkConfirm(request.POST['password'], request.POST['confirm']):
            allFieldsAreLegal = False
            reg_dict['confirmError'] = "Password confirmation does not match the password"
            reg_dict['confirmInput'] = request.POST['confirm']

        if request.POST['type'] == "":
            allFieldsAreLegal = False
        else:
            type = request.POST['type']

        # Check optional inputs. They could be left empty, but once filled they must be valid or will not be passed.
        email = ''
        if not validateEmail(request.POST['email']):
            allFieldsAreLegal = False
            reg_dict['emailError'] = "Please enter a valid email"
            reg_dict['emailInput'] = request.POST['email']
        else:
            email = request.POST['email']

        phoneNo = ''
        if not validatePhoneNo(request.POST['phoneNo']):
            allFieldsAreLegal = False
            reg_dict['phoneNoError'] = "Please enter a valid phone number"
            reg_dict['phoneNoInput'] = request.POST['phoneNo']
        else:
            phoneNo = request.POST['phoneNo']

        street = ''
        if not validateAddress(request.POST['street']):
            allFieldsAreLegal = False
            reg_dict['streetError'] = "Please enter a valid street number"
            reg_dict['streetInput'] = request.POST['street']
        else:
            street = request.POST['street']

        suburb = ''
        if not validateSuburb(request.POST['suburb']):
            allFieldsAreLegal = False
            reg_dict['suburbError'] = "Please enter a valid suburb"
            reg_dict['suburbInput'] = request.POST['suburb']
        else:
            suburb = request.POST['suburb']

        postcode = ''
        if not validatePostcode(request.POST['postcode']):
            allFieldsAreLegal = False
            reg_dict['postcodeError'] = "Please enter a valid postcode"
            reg_dict['postcodeInput'] = request.POST['postcode']
        else:
            postcode = request.POST['postcode']

		# Address inputs only if all fields are legal
        if allFieldsAreLegal:
			# Username cannot be the same as any other's
            if not existsUser(username):
				# Store the data in database to register
                registerUser(username, password, type, email, phoneNo, street, suburb, postcode)
				# Update session to login
                request.session['username'] = username
                request.session['type'] = type
                request.session['address'] = street + ' ' + suburb + ' ' + postcode
                request.session['isLogin'] = True
                return render(request, "account.html", getAccountDict(request))
			# Tell the user that the username has been used
            else:
                reg_dict = {}
                inputs = ['username','email','phoneNo','street','suburb','postcode']
                for input in inputs:
                    inputKey = input + 'Input'
                    errorKey = input + 'Error'
                    reg_dict[inputKey] = request.POST[input]
                    reg_dict[errorKey] = ''
                reg_dict['mainError'] = 'The username has been used, please use another one'
                return render(request, "register.html", reg_dict)
        else:
            return render(request, "register.html", reg_dict)
    else:
        return render(request, "register.html", reg_dict)

# The processing method for administrator registration page
# Author: Jason
def registerAdmin(request):
	# Only an administrator can register an administrator
    if request.session.get('type', False) == 'administrator':
		# Dictionary for administrator registration page
        reg_admin_dict ={}
        if request.POST:
            allFieldsAreLegal = True

            # Check required inputs. When any of them are not filled, or not valid, the form will not be passed,
			# and some error massage will be shown.
            if not validateUsername(request.POST['username']):
                allFieldsAreLegal = False
                reg_admin_dict['usernameError'] = "Please enter a valid username"
                reg_admin_dict['usernameInput'] = request.POST['username']
            else:
                username = request.POST['username']

            if not validatePassword(request.POST['password']):
                allFieldsAreLegal = False
                reg_admin_dict['passwordError'] = "Please enter a valid password"
                reg_admin_dict['passwordInput'] = request.POST['password']
            else:
                password = request.POST['password']

            if not checkConfirm(request.POST['password'], request.POST['confirm']):
                allFieldsAreLegal = False
                reg_admin_dict['confirmError'] = "Password confirmation does not match the password"
                reg_admin_dict['confirmInput'] = request.POST['confirm']

            # Check optional inputs. They could be left empty, but once filled they must be valid or will not be passed.
            email = ''
            if not validateEmail(request.POST['email']):
                allFieldsAreLegal = False
                reg_admin_dict['emailError'] = "Please enter a valid email"
                reg_admin_dict['emailInput'] = request.POST['email']
            else:
                email = request.POST['email']

            phoneNo = ''
            if not validatePhoneNo(request.POST['phoneNo']):
                allFieldsAreLegal = False
                reg_admin_dict['phoneNoError'] = "Please enter a valid phone number"
                reg_admin_dict['phoneNoInput'] = request.POST['phoneNo']
            else:
                phoneNo = request.POST['phoneNo']

			# Address inputs only if all fields are legal
            if allFieldsAreLegal:
				# Username cannot be the same as any other's
                if not existsUser(username):
                    type = "administrator"
					# Store the data in database to register
                    registerUser(username, password, type, email, phoneNo, None, None, None)
					# Update session to login
                    request.session['username'] = username
                    request.session['type'] = type
                    request.session['address'] = ''
                    request.session['isLogin'] = True
                    return render(request, "account.html", getAccountDict(request))
				# Tell the user that the username has been used
                else:
                    reg_admin_dict = {}
                    inputs = ['username','email','phoneNo']
                    for input in inputs:
                        inputKey = input + 'Input'
                        errorKey = input + 'Error'
                        reg_admin_dict[inputKey] = request.POST[input]
                        reg_admin_dict[errorKey] = ''
                    reg_admin_dict['mainError'] = 'The username has been used, please use another one'
                    return render(request, "registerAdmin.html", reg_admin_dict)
            else:
                return render(request, "registerAdmin.html", reg_admin_dict)
        else:
            return render(request, "registerAdmin.html", reg_admin_dict)
    else:
        return render(request, "login.html")

# The processing method for login page
# Author: Jason
def login(request):
	# Dictionary for login page
    login_dict = {}
    if request.POST:
		# Check if both inputs are valid
        if validateUsername(request.POST['username']) and validatePassword(request.POST['password']):
            username = request.POST['username']
            password = request.POST['password']
            if verifyUser(username, password):
				# Register session to login
                type = User.objects.get(username=username).type
                street = User.objects.get(username=username).street
                suburb = User.objects.get(username=username).suburb
                postcode = User.objects.get(username=username).postcode
                address = street + ' ' + suburb + ' ' + postcode
                request.session['username'] = username
                request.session['type'] = type
                request.session['address'] = address
                request.session['isLogin'] = True
                return render(request, "account.html", getAccountDict(request))
            else:
                login_dict['mainError'] = 'Username or password incorrect'
                login_dict['usernameInput'] = username
                login_dict['passwordInput'] = password
                login_dict['usernameError'] = ''
                login_dict['passwordError'] = ''
                return render(request, "login.html", login_dict)
        else:
            login_dict['usernameInput'] = request.POST['username']
            login_dict['passwordInput'] = request.POST['password']
            login_dict['usernameError'] = 'Please enter a valid username'
            login_dict['passwordError'] = 'Please enter a valid password'
            return render(request, "login.html", login_dict)
    else:
        return render(request, "login.html", login_dict)

# The processing method for logout
# Author: Jason
def logout(request):
    if request.session.get('isLogin', False) == True:
		# Delete all session data to logout
        del request.session['username']
        del request.session['type']
        del request.session['address']
        del request.session['isLogin']

    return redirect(landing_page)

# The processing method for Account page
# Author: Josh
def account(request):
    if request.session.get('isLogin', False) == False:
        return render(request, "account.html", {
            "isLogin": request.session.get('isLogin', False),
            "username": request.session.get('username', "ERROR"),
        })
    return redirect(landing_page)


def search(request):
    search = request.POST['search'].upper()
    context_dict = {}
    lists = []
    zoo_list = []
    parl_list = []
    museum_list = []
    restaurant_list = []
    mall_list = []
    college_list = []
    library_list = []
    hotel_list = []
    industry_list = []  
    
    if 'ZOO' in search:
        zoo_list = Zoo.objects.all()
        lists += zoo_list

    if 'MUSEUM' in search:
        museum_list = Museum.objects.all().order_by('-rating')
        lists += museum_list

    if 'RESTAURANT' in search:
        restaurant_list = Restaurant.objects.all()   
        lists += restaurant_list


    if 'MALL' in search:
        mall_list = Mall.objects.all().order_by('-rating')
        lists += mall_list
    
    if 'COLLEGE' in search:
        college_list = College.objects.all()
        # else:
        #     college_list = College.objects.filter(name__icontains=search).order_by('-rating')
        #     college_list = College.objects.filter(address__icontains=search).order_by('-rating')
        lists += college_list

    if 'LIBRARY' in search:
        library_list = Library.objects.all()
        # else:
        #     library_list = Library.objects.filter(name__icontains=search).order_by('-rating')
         #   mall_list = Mall.objects.filter(address__icontains='Brisbane City').order_by('-rating')
        lists += library_list

    if 'HOTEL' in search:
        hotel_list = Hotel.objects.all()
        # else:
        #     hotel_list = Hotel.objects.filter(name__icontains=search).order_by('-rating')
        #     hotel_list = Hotel.objects.filter(address__icontains=search).order_by('-rating')
        lists += hotel_list

    if 'INDUSTRY' in search:
        industry_list = Industry.objects.all()
        # else:
        #     industry_list = Industry.objects.filter(name__icontains=search).order_by('-rating')
        #     industry_list = Industry.objects.filter(address__icontains=search).order_by('-rating')
        lists += industry_list

    if 'PARK' in search:
        park_list = Park.objects.all()
        # else:
        #     park_list = Park.objects.filter(name__icontains=search).order_by('-rating')
        #     park_list = Park.objects.filter(address__icontains=search).order_by('-rating')
        lists += park_list
    
    
    context_dict = {'lists': lists,'search_term': request.POST['search']}
    
    if request.method == 'POST':
        return render(request, "search.html", context_dict)
        
    return render(request, "error.html")

# The processing method for changing password by verifying old password
# Author: Jason
def changePassword(request):
	# Dictionary for the change password page
    cpsw_dict = {}
    if request.POST:
        if validatePassword(request.POST['password']):
            username = request.session.get('username', False)
            password = request.POST['password']
                
            user = User.objects.get(username = username)
            # Revise the password with hashing
            user.password = make_password(password, None, 'pbkdf2_sha256')
            user.save()
            return render(request, "account.html", getAccountDict(request))
        else:
            cpsw_dict['passwordError'] = 'Please enter a valid password'
            return render(request, "changePassword.html", cpsw_dict)
    else:
        return render(request, "changePassword.html", cpsw_dict)
		
# The processing method for recovering password by verifying email and phone number
# Author: Jason
def recoverPassword(request):
    rpsw_dict = {}
    if request.POST:
        allFieldsAreLegal = True

        if not validateUsername(request.POST['username']):
            allFieldsAreLegal = False
            rpsw_dict['usernameError'] = "Please enter a valid username"
            rpsw_dict['usernameInput'] = request.POST['username']
        else:
            username = request.POST['username']

        email = ''
        if not validateEmail(request.POST['email']):
            allFieldsAreLegal = False
            rpsw_dict['emailError'] = "Please enter a valid email"
            rpsw_dict['emailInput'] = request.POST['email']
        else:
            email = request.POST['email']

        phoneNo = ''
        if not validatePhoneNo(request.POST['phoneNo']):
            allFieldsAreLegal = False
            rpsw_dict['phoneNoError'] = "Please enter a valid phone number"
            rpsw_dict['phoneNoInput'] = request.POST['phoneNo']
        else:
            phoneNo = request.POST['phoneNo']

        if not validatePassword(request.POST['password']):
            allFieldsAreLegal = False
            rpsw_dict['passwordError'] = 'Please enter a valid password'
        else:
            password = request.POST['password']

        if allFieldsAreLegal:
            if not existsUser(username):
                rpsw_dict['mainError'] = 'The username does not exist'
                return render(request, "recoverPassword.html", rpsw_dict)
            else:
                user = User.objects.get(username = username)
                if (user.email == email) and (user.phoneNo == phoneNo):
                    # Revise the password with hashing
                    user.password = make_password(password, None, 'pbkdf2_sha256')
                    user.save()
                    return render(request, "account.html", getAccountDict(request))
                else:
                    rpsw_dict['mainError'] = 'The email or phone number is not correct'
                    return render(request, "recoverPassword.html", rpsw_dict)
        else:
            return render(request, "recoverPassword.html", rpsw_dict)
    else:
        return render(request, "recoverPassword.html", rpsw_dict)

# The process method for account settings page
# Author: Jason
def accountSettings(request):
	# Display current user information by the dictionary
    username = request.session.get('username', False)
    user = User.objects.get(username = username)
    email = user.email
    street = user.street
    suburb = user.suburb
    postcode = user.postcode
    phoneNo = user.phoneNo
    acst_dict = {
        'emailInput':email,
        'streetInput':street,
        'suburbInput':suburb,
        'postcodeInput':postcode,
        'phoneNoInput':phoneNo,
        }
    if request.POST:
        allFieldsAreLegal = True

        type = request.POST['type']

		# Check all inputs. They could be empty, which will clean previous data, or they must be valid when they are filled.
        email = ''
        if not validateEmail(request.POST['email']):
            allFieldsAreLegal = False
            acst_dict['emailError'] = "Please enter a valid email"
            acst_dict['emailInput'] = request.POST['email']
        else:
            email = request.POST['email']

        phoneNo = ''
        if not validatePhoneNo(request.POST['phoneNo']):
            allFieldsAreLegal = False
            acst_dict['phoneNoError'] = "Please enter a valid phone number"
            acst_dict['phoneNoInput'] = request.POST['phoneNo']
        else:
            phoneNo = request.POST['phoneNo']

        street = ''
        if not validateAddress(request.POST['street']):
            allFieldsAreLegal = False
            acst_dict['streetError'] = "Please enter a valid street number"
            acst_dict['streetInput'] = request.POST['street']
        else:
            street = request.POST['street']

        suburb = ''
        if not validateSuburb(request.POST['suburb']):
            allFieldsAreLegal = False
            acst_dict['suburbError'] = "Please enter a valid suburb"
            acst_dict['suburbInput'] = request.POST['suburb']
        else:
            suburb = request.POST['suburb']

        postcode = ''
        if not validatePostcode(request.POST['postcode']):
            allFieldsAreLegal = False
            acst_dict['postcodeError'] = "Please enter a valid postcode"
            acst_dict['postcodeInput'] = request.POST['postcode']
        else:
            postcode = request.POST['postcode']

        if allFieldsAreLegal:
			# Update all data
            user = User.objects.get(username = username)
            user.type = type
            user.email = email
            user.phoneNo = phoneNo
            user.street = street
            user.suburb = suburb
            user.postcode = postcode
            user.save()
			
			# Update session
            address = street + ' ' + suburb + ' ' + postcode
            request.session['username'] = username
            request.session['type'] = type
            request.session['address'] = address
            return render(request, "account.html", getAccountDict(request))
        else:
            return render(request, "accountSettings.html", acst_dict)
    else:
        return render(request, "accountSettings.html", acst_dict)

# The process method for location page
# Author: Jason
def location(request):
    location = request.GET['location']
    if request.POST:
        if request.session.get('isLogin', False) == False:
            return render(request, "login.html")
        else:
            location = request.GET['location']
            username = request.session.get('username', False)
            review = request.POST['review']
            Review.objects.create(location=location, username=username, content=review)
            return render(request, "location.html", getLocationDict(request, location))
    else:
        return render(request, "location.html", getLocationDict(request, location))

def getLocationDict(request):
    location = request.session.get('location')
    reviews = Review.objects.get(location=location)
    location_dict = {
        'location': location,
        'reviews':reviews,
    }
    return location_dict

# The method to obtain the dictionary for account page
# Author: Jason
def getAccountDict(request):
    username = request.session.get('username', False)
    type = request.session.get('type', False)
    address = request.session.get('address', False)
    account_dict = {
        'username':username,
        'type':type,
        'address':address,
        }
    return account_dict
	
# Check if the user is an administrator
def isAdmin(request):
    if (request.session.get('type', False) == 'administrator'):
        return True
    else:
        return False
	
# The processing method to modify reviews
# Author: Jason
def modifyReview(request):
    reviewID = request.GET['reviewID']
    review = Review.objects.get(ID=reviewID)
	# Display the review's information
    mr_dict = {
        'username':review.username,
        'location':review.location,
        'rating':review.rating,
        'time':review.time,
        'content':review.content,
        }
    if request.POST:
		# Revise the content
        newContent = request.POST['content']
        review.update(content=newContent)
        return render(request, "review.html", getReviewDict(request, location))
    else:
        return render(request, "modifyReview.html", mr_dict)

# The method to delete a review
# Author: Jason
def deleteReview(request):
    reviewID = request.GET['reviewID']
    review = Review.objects.get(ID=reviewID)
    location = review.location
    review.delete()
    return render(request, "review.html", getReviewDict(request, location))


