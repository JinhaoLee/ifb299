from django.shortcuts import render
from django.contrib.auth.hashers import make_password, check_password
from re import *

from users.models import User

# Check if an input is set
def isSet(input):
    try:
        type(eval(input))
    except:
        return False
    else:
        return True

# Check if an input is empty
def isEmpty(input):
    if input.strip(' ') == '':
        return True
    else:
        return False

# Check if an input is filled
def isFilled(input):
    if isSet(input) and (not isEmpty(input)):
        return True
    else:
        return False

# Check if the username is valid
def validateUsername(username):
    pattern = r'^[a-zA-Z0-9._%+-]+$'
    minLength = 6
    maxLength = 15

    if findall(pattern, username):
        if (len(username) >= minLength) and (len(username) <= maxLength):
            return True
        else:
            return False
    else:
        return False

# Check if the password is valid
def validatePassword(password):
    pattern = r'^[a-zA-Z0-9._%#$^&*]+$'
    minLength = 6
    maxLength = 15

    if findall(pattern, password):
        if (len(password) >= minLength) and (len(password) <= maxLength):
            return True
        else:
            return False
    else:
        return False

# Check if the email is valid
def validateEmail(email):
    pattern = r'^[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$'
    maxLength = 40

    if findall(pattern, email):
        if len(email) <= maxLength:
            return True
        else:
            return False
    elif not isFilled(email):
        return True
    else:
        return False

# Check if the password confirm matches password
def checkConfirm(password, confirm):
    if password == confirm:
        return True
    else:
        return False

# Check if the phone number is valid
def validatePhoneNo(phoneNo):
    pattern = r'^\d+$'
    minLength = 6
    maxLength = 15

    if findall(pattern, phoneNo):
        if (len(phoneNo) >= minLength) and (len(phoneNo) <= maxLength):
            return True
        else:
            return False
    elif not isFilled(phoneNo):
        return True
    else:
        return False

# Check if the address is valid
def validateAddress(address):
    pattern = r'^[0-9]+ [a-zA-Z ]+$'
    maxLength = 40

    if findall(pattern, address):
        if len(address) <= maxLength:
            return True
        else:
            return False
    elif not isFilled(address):
        return True
    else:
        return False

# Check if the suburb is valid
def validateSuburb(suburb):
    pattern = r'^[a-zA-Z]+[a-zA-Z ]*$'
    maxLength = 40

    if findall(pattern, suburb):
        if len(suburb) <= maxLength:
            return True
        else:
            return False
    elif not isFilled(suburb):
        return True
    else:
        return False

# Check if the postcode is valid
def validatePostcode(postcode):
    pattern = r'^[0-7]{1}[0-9]{3}$'

    if findall(pattern, postcode):
        return True
    elif not isFilled(postcode):
        return True
    else:
        return False

# Check if the user has existed		
def existsUser(username):
    try:
        if User.objects.get(username=username):
            return True
        else:
            return False
    except:
        return False

# Register the user by storing data into database
def registerUser(username, password, type, email, phoneNo, street, suburb, postcode):
    hashedPassword = make_password(password, None, 'pbkdf2_sha256')
    User.objects.create(username = username, password = hashedPassword, type = type, email = email, 
                        phoneNo = phoneNo, street = street, suburb = suburb, postcode = postcode)

# Verify a user from user database
def verifyUser(username, password):
    if existsUser(username=username):
        user = User.objects.get(username=username)
        hashedPassword = user.password
        if check_password(password, hashedPassword):
            return True
        else:
            return False
    else:
        return False

