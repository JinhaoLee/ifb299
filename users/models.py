from django.db import models

# This model file is to establish a user database
# Author: Jason

class User(models.Model):
    username = models.CharField(max_length = 128, db_column = 'username', primary_key = True)
    password = models.CharField(max_length = 128, db_column = 'password', blank = False)
    type = models.CharField(max_length = 15, db_column = 'type', blank = False)
    email = models.CharField(max_length = 128, db_column = 'email')
    phoneNo = models.CharField(max_length = 60, db_column = 'phoneNo')
    street = models.CharField(max_length = 60, db_column = 'street')
    suburb = models.CharField(max_length = 128, db_column = 'suburb')
    postcode = models.CharField(max_length = 4, db_column = 'postcode')

    def __str__(self):
        return self.username
		
class Review(models.Model):
    reviewID = models.AutoField(db_column = 'reviewID', primary_key = True)
    username = models.CharField(max_length = 15, db_column = 'username', blank = False)
    location = models.CharField(max_length = 15, db_column = 'location', blank = False)
    content = models.TextField(db_column = 'content', blank = False)
    rating = models.CharField(max_length = 3, db_column = 'rating', blank = False)
    time = models.DateField(db_column = 'time', auto_now_add = True)