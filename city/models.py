# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models

# Create your models here.

# 1. Tourist Database

class Park(models.Model):
	park_id = models.AutoField(primary_key=True)
	name = models.CharField(max_length=128)
	address = models.CharField(max_length=128)
	phone = models.CharField(max_length=128)
	website = models.CharField(max_length=128)
	rating = models.DecimalField(max_digits=5, decimal_places=1, default=0.0)
	def __str__(self):
		return self.name

class Zoo(models.Model):
	zoo_id = models.AutoField(primary_key=True)
	name = models.CharField(max_length=128)
	address = models.CharField(max_length=128)
	phone = models.CharField(max_length=128)
	website = models.CharField(max_length=128)
	rating = models.DecimalField(max_digits=5, decimal_places=1, default=0.0)
	def __str__(self):
		return self.name

class Museum(models.Model):
	museum_id = models.AutoField(primary_key=True)
	name = models.CharField(max_length=128)
	address = models.CharField(max_length=128)
	phone = models.CharField(max_length=128)
	website = models.CharField(max_length=128)
	rating = models.DecimalField(max_digits=5, decimal_places=1, default=0.0)
	def __str__(self):
		return self.name

class Restaurant(models.Model):
	restaurant_id = models.AutoField(primary_key=True)
	name = models.CharField(max_length=128)
	address = models.CharField(max_length=128)
	phone = models.CharField(max_length=128)
	website = models.CharField(max_length=128)
	rating = models.DecimalField(max_digits=5, decimal_places=1, default=0.0)
	def __str__(self):
		return self.name

class Mall(models.Model):
	mall_id = models.AutoField(primary_key=True)
	name = models.CharField(max_length=128)
	address = models.CharField(max_length=128)
	phone = models.CharField(max_length=128)
	website = models.CharField(max_length=128)
	rating = models.DecimalField(max_digits=5, decimal_places=1, default=0.0)
	def __str__(self):
		return self.name

class College(models.Model):
	college_id = models.AutoField(primary_key=True)
	name = models.CharField(max_length=128)
	address = models.CharField(max_length=128)
	phone = models.CharField(max_length=128)
	website = models.CharField(max_length=128)
	rating = models.DecimalField(max_digits=5, decimal_places=1, default=0.0)

	def __str__(self):
		return self.name


class Library(models.Model):
	class Meta:
		verbose_name_plural = "libraries"
	library_id = models.AutoField(primary_key=True)
	name = models.CharField(max_length=128)
	address = models.CharField(max_length=128)
	phone = models.CharField(max_length=128)
	website = models.CharField(max_length=128)
	rating = models.DecimalField(max_digits=5, decimal_places=1, default=0.0)

	def __str__(self):
		return self.name

class Hotel(models.Model):
	hotel_id = models.AutoField(primary_key=True)
	name = models.CharField(max_length=128)
	address = models.CharField(max_length=128)
	phone = models.CharField(max_length=128, default = 'null')
	website = models.CharField(max_length=128, default = 'null')
	rating = models.DecimalField(max_digits=5, decimal_places=1, default=0.0)

	def __str__(self):
		return self.name

class Industry(models.Model):
	class Meta:
		verbose_name_plural = "Industries"
	industry_id = models.AutoField(primary_key=True)
	name = models.CharField(max_length=128)
	address = models.CharField(max_length=128)
	phone = models.CharField(max_length=128)
	website = models.CharField(max_length=128)
	rating = models.DecimalField(max_digits=5, decimal_places=1, default=0.0)

	def __str__(self):
		return self.name