from django import forms
from city.models import Mall

class MallForm(forms.ModelForm):
	name = forms.CharField(max_length=128,
		help_text="Please enter the mall name.")
	address = forms.CharField(widget=forms.HiddenInput())
	phone = forms.CharField(widget=forms.HiddenInput())
	website = forms.CharField(widget=forms.HiddenInput())
	rating = forms.DecimalField(widget=forms.HiddenInput())

    # An inline class to provide additional information on the form.
	class Meta:
		# Provide an association between the ModelForm and a model 
		model = Category
		fields = ('name',)