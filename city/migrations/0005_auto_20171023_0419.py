# -*- coding: utf-8 -*-
# Generated by Django 1.11.5 on 2017-10-23 04:19
from __future__ import unicode_literals

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('city', '0004_auto_20171022_1141'),
    ]

    operations = [
        migrations.RenameField(
            model_name='college',
            old_name='department',
            new_name='phone',
        ),
        migrations.RenameField(
            model_name='industry',
            old_name='hotel_id',
            new_name='industry_id',
        ),
    ]
