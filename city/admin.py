# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib import admin

from city.models import *
# Register your models here.

class Admin(admin.ModelAdmin):
	list_display = ('name', 'address', 'phone','website', 'rating')

admin.site.register(Park, Admin)
admin.site.register(Zoo)
admin.site.register(Museum, Admin)
admin.site.register(Restaurant, Admin)
admin.site.register(Mall, Admin)
admin.site.register(College)
admin.site.register(Library)
admin.site.register(Hotel, Admin)
admin.site.register(Industry)